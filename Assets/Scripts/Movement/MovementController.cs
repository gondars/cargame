﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using CnControls;

public class MovementController : MonoBehaviour 
{

    public WheelCollider tireBackL;
    public WheelCollider tireBackR;
    public WheelCollider tireFrontL;
    public WheelCollider tireFrontR;
    public Light brakeLightL;
    public Light brakeLightR;
    public float maxTorque;
    public float speed;
    public float steeringAngle;


    // Use this for initialization
    void Awake()
    {
        tireBackL = transform.GetChild(1).GetComponent<WheelCollider>();
        tireBackR = transform.GetChild(2).GetComponent<WheelCollider>();
        tireFrontL = transform.GetChild(3).GetComponent<WheelCollider>();
        tireFrontR = transform.GetChild(4).GetComponent<WheelCollider>();
        brakeLightL = transform.GetChild(0).GetChild(0).GetComponent<Light>();
        brakeLightR = transform.GetChild(0).GetChild(1).GetComponent<Light>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        tireBackR.motorTorque = speed * maxTorque * CnInputManager.GetAxis("Vertical");
        tireBackL.motorTorque = speed * maxTorque * CnInputManager.GetAxis("Vertical");
        tireFrontR.steerAngle = speed * steeringAngle * CnInputManager.GetAxis("Horizontal");
        tireFrontL.steerAngle = speed * steeringAngle * CnInputManager.GetAxis("Horizontal");
    }

    void Update()
    {
        StopLights();
    }

    void StopLights()
    {
        if (CnInputManager.GetAxis("Vertical") < 0)
        {
            if (!brakeLightL.enabled && !brakeLightR.enabled)
            {
                brakeLightR.enabled = true;
                brakeLightL.enabled = true;
            }
        }
        else
        {
            if (brakeLightL.enabled && brakeLightR.enabled)
            {
                brakeLightR.enabled = false;
                brakeLightL.enabled = false;
            }
        }
    }
}
