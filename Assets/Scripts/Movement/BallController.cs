﻿using UnityEngine;
using System.Collections;

public class BallController : MonoBehaviour {

    private Rigidbody rigidBody;

    private Transform spawnPoint;

    void Awake()
    {
        rigidBody = GetComponent<Rigidbody>();
        spawnPoint = GameObject.FindGameObjectWithTag(Constants.SPAWNPOINT_BALL).GetComponent<Transform>();
    }

    void Start()
    {
        transform.position = spawnPoint.transform.position;
    }
	// Use this for initialization
	void OnTriggerEnter(Collider col)
    {
        if(col.gameObject.tag == Constants.GOAL_RIGHT)
        {
            GameManager.Instance.GoalScored(TeamType.RED);

        }
        else if (col.gameObject.tag == Constants.GOAL_LEFT)
        {
            GameManager.Instance.GoalScored(TeamType.BLUE);
        }
        
    }

    public void resetBallPosition()
    {
        transform.position = spawnPoint.position;
        transform.rotation = Quaternion.Euler(Vector3.zero);
        rigidBody.velocity = Vector3.zero;
        rigidBody.angularVelocity = Vector3.zero;

    }
}
