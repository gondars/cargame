﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using CnControls;
using UnityEngine.EventSystems;

public class ArcadeMovement : MonoBehaviour 
{
    private Rigidbody rb;
    private Light brakeLightL;
    private Light brakeLightR;
    public Button gasBtn;
    public Button breaksBtn;

    public float velocity = 22f;
    public float rotateAngle;
    public float friction;
    public bool MovingBackwards;
    public bool movementLocked = false;
    public static bool gasBtnClicked;
    public static bool breaksBtnClicked;
    public float horizontalAxis = 0f;
    public Transform Spawnpoint;
	// Use this for initialization

	void Awake () 
    {
        rb = GetComponent<Rigidbody>();
        brakeLightL = transform.GetChild(0).GetChild(0).GetComponent<Light>();
        brakeLightR = transform.GetChild(0).GetChild(1).GetComponent<Light>();
       
	}

	// Update is called once per frame
	void FixedUpdate ()
    {
        if(!movementLocked)
        {
            VelocityMovement();
            AngularRotate();
            Stoplights();
            CheckIfMovingBackwards();
            ButtonControls();
        }
	}

    private void ButtonControls()
    {
        if (gasBtnClicked)
        {
            rb.velocity += transform.forward * velocity / 100 * horizontalAxis;
            horizontalAxis += 0.05f;
            if (horizontalAxis > 1)
            {
                horizontalAxis = 1;
            }
        }
        if(breaksBtnClicked)
        {
            rb.velocity += transform.forward * velocity / 100 * horizontalAxis;
            horizontalAxis -= 0.05f;
            if(horizontalAxis < -1)
            {
                horizontalAxis = -1;
            }
        }
    }   

    public void OnGasPointerDown()
    {
        gasBtnClicked = true;
    }

    public void OnGasPointerUp()
    {
        gasBtnClicked = false;
    }

    public void OnBreaksPointerDown()
    {
        breaksBtnClicked = true;
    }

    public void OnBreaksPointerUp()
    {
        breaksBtnClicked = false;
    }

    void VelocityMovement()
    {
         rb.velocity += transform.forward * velocity / 100 * CnInputManager.GetAxis(Constants.AXIS_VERTICAL);
    }

    void AngularRotate()
    {
        
            if (rb.velocity != Vector3.zero)
            {
                if(!MovingBackwards)
                {
                    Vector3 reqAngle = new Vector3(0, CnInputManager.GetAxis(Constants.AXIS_HORIZONTAL) * (rb.velocity.magnitude / friction) * rotateAngle, 0);
                    Quaternion deltaRotation = Quaternion.Euler(reqAngle * Time.deltaTime);
                    rb.MoveRotation(rb.rotation * deltaRotation);
                }
                else
                {
                    Vector3 reqAngle = new Vector3(0, (CnInputManager.GetAxis(Constants.AXIS_HORIZONTAL)* -1) * (rb.velocity.magnitude / friction) * rotateAngle, 0);
                    Quaternion deltaRotation = Quaternion.Euler(reqAngle * Time.deltaTime);
                    rb.MoveRotation(rb.rotation * deltaRotation);
                }
                
            }
        
        
    }

    void Stoplights()
    {
        if (Input.GetAxis(Constants.AXIS_VERTICAL) < 0)
        {
            if (!brakeLightL.enabled && !brakeLightR.enabled)
            {
                brakeLightR.enabled = true;
                brakeLightL.enabled = true;
            }
        }
        else
        {
            if (brakeLightL.enabled && brakeLightR.enabled)
            {
                brakeLightR.enabled = false;
                brakeLightL.enabled = false;
            }
        }
    }

    void CheckIfMovingBackwards()
    {
        if (Input.GetAxis(Constants.AXIS_VERTICAL) < 0)
        {
            MovingBackwards = true;
        }
        else
        {
            MovingBackwards = false;
        }
    }

    public void SetSpawnpoint(Transform t)
    {
        Spawnpoint = t;
    }

    public void ResetPosition()
    {
        this.transform.position = Spawnpoint.position;
        this.transform.rotation = Spawnpoint.rotation;
        this.rb.velocity = Vector3.zero;
        this.rb.angularVelocity = Vector3.zero;
    }

    public void ToggleMovementLock()
    {
        if(movementLocked)
        {
            movementLocked = false;
        }
        else
        {
            movementLocked = true;
        }
    }

    public void ActivateBoost()
    {
        velocity = 30f;
        Debug.Log("BoostActivated");

    }

    public void DeatctivateBoost()
    {
        velocity = 22f;
        Debug.Log("BoostDeactivated");
    }

    void OnTriggerEnter (Collider col)
    {
        if(col.gameObject.tag == Constants.BOOST)
        {
            ActivateBoost();
            StartCoroutine("BoostActivated");
        }
    }
    IEnumerator BoostActivated()
    {
        yield return new WaitForSeconds(3f);
        DeatctivateBoost();

    }
}
