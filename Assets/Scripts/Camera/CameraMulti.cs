﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraMulti : MonoBehaviour
{
    public List<GameObject> list;

    private GameObject ball;
    private GameObject[] players;

    Camera mainCamera;
    float cameraSmooth = 1f;

    public float XMin = 0f;
    public float XMax = 0f;
    public float ZMin = 0f;
    public float ZMax = 0f;

	// Use this for initialization
	void Awake ()
    {
        mainCamera = Camera.FindObjectOfType<Camera>();
        ball = GameObject.FindGameObjectWithTag("Ball");
        players = GameObject.FindGameObjectsWithTag("Player");

        list = new List<GameObject>();
	}

    void Start()
    {
        for (int i = 0; i < players.Length; i++)
        {
            list.Add(players[i]);
        }
        list.Add(ball);
        
    }
	
	// Update is called once per frame
	void Update () 
    {
        findPos();
	}

    void findPos()
    {
        FindMinX();
        FindMaxX();
        FindMinZ();
        FindMaxZ();

        float xCenter = XMin + (Mathf.Abs(XMax - XMin) / 2F);
        float zCenter = ZMin + (Mathf.Abs(ZMax - ZMin) / 2F);

        float height = Mathf.Abs( 90f / mainCamera.fieldOfView * 1.5f * zCenter - ZMax * 4f );

        Vector3 unSmoothCameraPos = new Vector3(xCenter, height , zCenter);

        mainCamera.transform.position = Vector3.Lerp(mainCamera.transform.position, unSmoothCameraPos, Time.deltaTime * cameraSmooth);

        
        Debug.Log(zCenter + " " + xCenter +" "+ height );
        

    }
    void FindMinX()
    {
        float minX = list[0].transform.position.x;
        for (int i = 0; i < list.Count; i++)
        {
            if(minX > list[i].transform.position.x)
            {
                minX = list[i].transform.position.x;
            }
        }

        XMin = Mathf.Round(minX);
    }

    void FindMaxX()
    {
        float maxX = list[0].transform.position.x;

        for (int i = 0; i < list.Count; i++)
        {
            if (maxX < list[i].transform.position.x)
            {
                maxX = list[i].transform.position.x;
            }
        }

        XMax = Mathf.Round(maxX);

    }
    void FindMinZ()
    {
        float minZ = list[0].transform.position.z;

        for (int i = 0; i < list.Count; i++)
        {
            if (minZ > list[i].transform.position.z)
            {
                minZ = list[i].transform.position.z;
            }
        }

        ZMin = Mathf.Round(minZ);
    }

    void FindMaxZ()
    {
        float maxZ = list[0].transform.position.z;

        for (int i = 0; i < list.Count; i++)
        {
            if (maxZ < list[i].transform.position.z)
            {
                maxZ = list[i].transform.position.z;
            }
        }

        ZMax = Mathf.Round(maxZ);
    }
}
