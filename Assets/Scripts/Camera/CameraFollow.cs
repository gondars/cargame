﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour 
{
    public Transform target;
    public float smoothTime = 1.5f;

    private Vector3 velocity = Vector3.zero;
    void Awake()
    {
         target = GameObject.FindGameObjectWithTag(Constants.PLAYER).transform;
        //target = GameObject.FindGameObjectWithTag(Constants.PLAYER).transform.GetChild(0).transform;
    }
	// Use this for initialization
    void Start()
    {
        transform.position = new Vector3(target.position.x ,10f,-8f);
    }
	
	// Update is called once per frame
	void LateUpdate () 
    {
        Vector3 goalPos = target.position ;
        goalPos.y = target.position.y + 10f;
        goalPos.z = target.position.z - 8f;
        //transform.position = Vector3.SmoothDamp(transform.position, goalPos, ref velocity, smoothTime);
        transform.position = Vector3.Lerp(transform.position, goalPos, smoothTime * Time.deltaTime);
       
	}
}
