﻿using UnityEngine;
using System.Collections;

public class BoostSpawner2 : MonoBehaviour {

    private GameObject boost;

    void Start()
    {
        SpawnBoost();
    }

    void SpawnBoost()
    {
        boost = GameObject.FindGameObjectWithTag(Constants.BOOST);
        Instantiate(boost, transform.position, Quaternion.identity);
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == Constants.PLAYERCOLLIDER)
        {
            StartCoroutine("SpawnBoostWithDelay");
        }
    }
    IEnumerator SpawnBoostWithDelay()
    {
        yield return new WaitForSeconds(10f);
        SpawnBoost();
    }
}
