﻿using UnityEngine;
using System.Collections;

public class BoostBehaviour : MonoBehaviour {

    public static bool noBoostsOnField = false;

	void OnTriggerEnter (Collider col)
    {
        if(col.gameObject.tag == Constants.PLAYERCOLLIDER)
        {
            Destroy(gameObject);
        }
    }
}
