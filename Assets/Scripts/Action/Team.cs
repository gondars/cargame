﻿using UnityEngine;
using System.Collections;

public class Team
{
    public TeamType teamType {get; set;}
    public int score { get; protected set; }

    public Team (TeamType t)
    {
        this.teamType = t;
        score = 0;
    }

    public void addScore()
    {
        score++;
    }
}
