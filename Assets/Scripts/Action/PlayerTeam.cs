﻿using UnityEngine;
using System.Collections;

public class PlayerTeam : MonoBehaviour 
{
    public TeamType team { get; protected set; }
    public int playerNumber {get; protected set;}


    public void SetPlayerNumber(int n)
    {
        this.playerNumber = n;
    }

    public void SetTeam(TeamType t)
    {
        this.team =t;
    }
	
}
