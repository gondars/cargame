﻿using UnityEngine;
using System.Linq;


public class PlayerManager : Singleton<PlayerManager> 
{
    protected PlayerManager() { }

    public GameObject playerPrefab;

    public GameObject[] spawnpoints;
    public GameObject[] players;

    private GameObject playersGOLoc;
    public Camera mainCamera;
    public CameraFollow cameraFollowScript;

    public bool playersSpawned = false;
    
    void Awake()
    {
        spawnpoints = GameObject.FindGameObjectsWithTag(Constants.SPAWNPOINT_PLAYER).OrderBy(go => go.name).ToArray();
        playersGOLoc = GameObject.FindGameObjectWithTag(Constants.PLAYERS_PARENT);
        mainCamera = Camera.FindObjectOfType<Camera>();
    }
    
    void Start()
    {
        SpawnPlayers();
        TeamManager.Instance.ArrangeTeams();
    }

    public void SpawnPlayers()
    {
        Transform targetSpawnpoint = spawnpoints[players.Length].transform;

        GameObject player = (GameObject)Instantiate(playerPrefab, Vector3.zero, Quaternion.identity);
        player.transform.parent = playersGOLoc.transform;
        player.transform.tag = Constants.PLAYER;
        player.GetComponent<ArcadeMovement>().SetSpawnpoint(targetSpawnpoint);
        player.GetComponent<ArcadeMovement>().ResetPosition();
        player.GetComponent<PlayerTeam>().SetPlayerNumber(players.Length);

        if(player.GetComponent<PlayerTeam>().playerNumber < 2)
        {
            player.GetComponent<PlayerTeam>().SetTeam(TeamType.BLUE);
        }

        else
        {
            player.GetComponent<PlayerTeam>().SetTeam(TeamType.RED);
        }

        mainCamera.gameObject.AddComponent<CameraFollow>();
        Debug.Log("Spawning Player: " + player.GetComponent<PlayerTeam>().playerNumber + " on " + player.GetComponent<PlayerTeam>().team + " team." );
        FetchPlayers();
    }

    public void FetchPlayers()
    {
        players = GameObject.FindGameObjectsWithTag(Constants.PLAYER);
    }

    public void ResetPlayerPosition()
    {
        foreach(GameObject p in players)
        {
            p.GetComponent<ArcadeMovement>().ResetPosition();
        }
    }

    public void LockMovement()
    {
        foreach(GameObject p in players)
        {
            p.GetComponent<ArcadeMovement>().ToggleMovementLock();
        }
    } 
	
}
