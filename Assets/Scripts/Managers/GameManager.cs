﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public delegate void OnStateChangeHandler();

public class GameManager : Singleton<GameManager>
{
    protected GameManager() { }

    private Text blueTeamScoreText;
    private Text redTeamScoreText;
    private Text timer;
    private Text scoreEventText;
    private GameObject overtimePanel;
    private GameObject ball;
    private GameState state;
    private GameObject fx_Paper;

    public float roundTimer = 120f;
    public float roundDelayTimer = 3f;
    public bool roundDelay = false;
    public bool gameHasEnded = false;
    public bool fulltimeExpired = false;

    public void Awake()
    {
        //initializing the scoreboard canvas
        timer = GameObject.FindGameObjectWithTag(Constants.GUI_TIMER).GetComponent<Text>();
        blueTeamScoreText = GameObject.FindGameObjectWithTag(Constants.GUI_BLUETEAMSCORE).GetComponent<Text>();
        redTeamScoreText = GameObject.FindGameObjectWithTag(Constants.GUI_REDTEAMSCORE).GetComponent<Text>();
        scoreEventText = GameObject.FindGameObjectWithTag(Constants.GUI_SCORE_EVENT_TEXT).GetComponent<Text>();
        overtimePanel = GameObject.FindGameObjectWithTag(Constants.GUI_OVERTIME);

        ball = GameObject.FindGameObjectWithTag(Constants.BALL);
        fx_Paper = GameObject.FindGameObjectWithTag(Constants.FX_PAPER);

        fx_Paper.GetComponent<EllipsoidParticleEmitter>().maxEmission = 0;
        scoreEventText.enabled = false;
        overtimePanel.SetActive(false);

        blueTeamScoreText.text = TeamManager.Instance.blueTeamScore + "";
        redTeamScoreText.text = TeamManager.Instance.redTeamScore + "";
    }

    public void OnBlueTeamScore()
    {
        TeamManager.Instance.blueTeam.addScore();
        Debug.Log("Blue Team Scored! Their score is: " + TeamManager.Instance.blueTeamScore);
        OnScoreGUI(TeamManager.Instance.blueTeam.teamType);
    }

    public void OnRedTeamScore()
    {
        TeamManager.Instance.redTeam.addScore();
        Debug.Log("Red Team Scored! Their score is: " + TeamManager.Instance.redTeamScore);
        OnScoreGUI(TeamManager.Instance.redTeam.teamType);
    }

    void OnScoreGUI(TeamType tt)
    {
        scoreEventText.text = tt.ToString() + " HAS SCORED";
        if(tt == TeamType.RED)
        {
            scoreEventText.color = Color.red;
        }
        else if(tt == TeamType.BLUE)
        {
            scoreEventText.color = Color.blue;
        }

        scoreEventText.enabled = true;
        fx_Paper.GetComponent<EllipsoidParticleEmitter>().maxEmission = 100;
    }

    void FixedUpdate()
    {
        if(!gameHasEnded)
        {
            UpdateScoreboard();
            UpdateTimer();
        }
        
        
    }

    void Update()
    {
        RoundDelay();
    }

    void UpdateScoreboard()
    {
        blueTeamScoreText.text = TeamManager.Instance.blueTeamScore + "";
        redTeamScoreText.text = TeamManager.Instance.redTeamScore + "";
    }

    void UpdateTimer()
    {
        timer.text = (int) roundTimer + "";
        if(!roundDelay)
        {
            roundTimer -= Time.deltaTime;
        }
        
        if (roundTimer < 0)
        {
            CheckEndResult();
        }
        
    }

    void CheckEndResult()
    {
        if (TeamManager.Instance.blueTeamScore == TeamManager.Instance.redTeamScore && fulltimeExpired == false)
        {
                fulltimeExpired = true;
                Overtime();
        }
        else
        {
            GameOver();
        }
    }

    void Overtime()
    {
        overtimePanel.SetActive(true);
        roundTimer = 30f;
    }

    public void GoalScored(TeamType tt)
    {
        if(!gameHasEnded)
        {
            if (tt == TeamType.RED)
            {
                TeamManager.Instance.redTeam.addScore();
            }
            else if (tt == TeamType.BLUE)
            {
                TeamManager.Instance.blueTeam.addScore();

            }
            Debug.Log(tt.ToString() + " Team Scored! Their score is: " + TeamManager.Instance.blueTeamScore);
            OnScoreGUI(tt);
            ball.GetComponent<BallController>().resetBallPosition();
            PlayerManager.Instance.ResetPlayerPosition();
            PlayerManager.Instance.LockMovement();

            roundDelay = true;
        }
        

    }

    void RoundDelay()
    {
        if(roundDelay)
        {
            
            roundDelayTimer -= Time.deltaTime;
            if (roundDelayTimer <= 0)
            {
                PlayerManager.Instance.LockMovement();
                roundDelay = false;
                scoreEventText.enabled = false;
                roundDelayTimer = 3f;
                fx_Paper.GetComponent<EllipsoidParticleEmitter>().maxEmission = 0;
            }
            Debug.Log(roundDelayTimer);
            
        }
        
    }
    void GameOver()
    {
        if(!gameHasEnded)
        {
            if (TeamManager.Instance.redTeamScore > TeamManager.Instance.blueTeamScore)
            {
                scoreEventText.text = TeamManager.Instance.redTeam.teamType + " Team Has Won!";
            }
            if (TeamManager.Instance.redTeamScore < TeamManager.Instance.blueTeamScore)
            {
                scoreEventText.text = TeamManager.Instance.blueTeam.teamType + "Team Has Won!";
            }
            if (TeamManager.Instance.redTeamScore == TeamManager.Instance.blueTeamScore)
            {
                scoreEventText.color = Color.white;
                scoreEventText.text ="DRAW";
            }
            PlayerManager.Instance.LockMovement();
            scoreEventText.enabled = true;
            fx_Paper.GetComponent<EllipsoidParticleEmitter>().maxEmission = 100;
            gameHasEnded = true;
            Time.timeScale = 0f;
        }
        
    }
}
