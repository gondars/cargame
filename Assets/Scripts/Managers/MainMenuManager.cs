﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class MainMenuManager : Singleton<MainMenuManager> 
{

    public Button btn;
    private bool sceneUpdateRequired = false;
    public bool sceneUpdateCompleted = false;
    private bool startCheckingLoadLevel = false;

	// Use this for initialization
	void Awake ()
    {
        btn = GameObject.FindGameObjectWithTag("GUI_StartButton").GetComponent<Button>();
        btn.onClick.AddListener(StartGameBTN);
	}
	
	// Update is called once per frame

    
	void Update () 
    {

	}

    void StartGameBTN()
    {
        LoadLevel(1);
    }

    public static void LoadLevel(int index)
    {
        if(Application.isLoadingLevel)
        {
            Debug.Log(index);
        }
    }
}
