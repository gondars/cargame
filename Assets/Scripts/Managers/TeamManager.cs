﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TeamManager : Singleton<TeamManager> 
{

    protected TeamManager() { }

    public Team blueTeam;
    public Team redTeam;

    public int redTeamScore = 0;
    public int blueTeamScore = 0;

    public List<GameObject> blueTeamPlayers;
    public List<GameObject> redTeamPlayers;


    void Awake()
    {
        blueTeam = new Team(TeamType.BLUE);
        redTeam = new Team(TeamType.RED);
        blueTeamPlayers = new List<GameObject>();
        redTeamPlayers = new List<GameObject>();
    }

    void Start()
    {

    }
    void FixedUpdate()
    {
        redTeamScore = redTeam.score;
        blueTeamScore = blueTeam.score;
    }

    public void ArrangeTeams()
    {
        foreach(GameObject p in PlayerManager.Instance.players)
        {
            if(p.GetComponent<PlayerTeam>().team == TeamType.BLUE)
            {
                blueTeamPlayers.Add(p);
            }
            else
            {
                redTeamPlayers.Add(p);
            }
        }
    }



}
