﻿using UnityEngine;
using System.Collections;

public enum GameState
{
    INTRO,
    MAIN_MENU,
    INGAME
}
