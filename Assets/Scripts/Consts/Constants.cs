﻿
public static class Constants
{
    //Player and Game Consts
    public const string PLAYER = "Player";
    public const string BALL = "Ball";
    public const string BOOST = "Boost";
    public const string SPAWNPOINT_PLAYER = "Spawnpoint";
    public const string SPAWNPOINT_BALL = "BallSpawnpoint";
    public const string PLAYERS_PARENT = "PlayersParent";
    public const string PLAYERCOLLIDER = "PlayerCollider";
    public const string SPAWNPOINT_BOOST = "SpawnPointBoost";
    

    //BallController
    public const string GOAL_RIGHT = "RGoal";
    public const string GOAL_LEFT = "LGoal";

    //Axises

    public const string AXIS_VERTICAL = "Vertical";
    public const string AXIS_HORIZONTAL = "Horizontal";

    //GUI
    public const string GUI_TIMER = "Timer";
    public const string GUI_BLUETEAMSCORE = "BlueTeamScore";
    public const string GUI_REDTEAMSCORE = "RedTeamScore";
    public const string GUI_OVERTIME = "Overtime";
    public const string GUI_SCORE_EVENT_TEXT = "ScoreEventText";

    //FX

    public const string FX_PAPER = "FX_Paper";
}

